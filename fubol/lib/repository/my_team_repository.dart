import 'dart:io';

import 'package:fubol/model/teams.dart';

abstract class MyTeamRepositoryBase {
  //Future<MyTeam?> getMyTeam();

  //Future<void> saveMyTeam(MyTeam team, File? image);

  Future<List<MyTeam>> getTeams();
}
