import 'dart:io';

import 'package:fubol/model/user.dart';

abstract class MyUserRepositoryBase {
  Future<MyUser?> getMyUser();

  Future<void> saveMyUser(MyUser user, File? image);

  Future<List<MyUser>> getUsers();
}
