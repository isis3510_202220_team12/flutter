import 'package:fubol/model/teams.dart';
import 'package:fubol/provider/firebase_provider.dart';
import 'dart:io';

import 'package:fubol/repository/my_team_repository.dart';

class MyTeamRepository extends MyTeamRepositoryBase {
  final provider = FirebaseProvider();

  //@override
  //Future<void> saveMyTeam(MyTeam team, File? image) =>
      //provider.saveMyTeam(team, image);

  @override
  Future<List<MyTeam>> getTeams() => provider.getTeams();
}
