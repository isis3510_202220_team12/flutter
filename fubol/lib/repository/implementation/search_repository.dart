import 'dart:convert';

import 'package:fubol/model/user.dart';
import 'package:http/http.dart' as http;

class SearchRepository{
  
  Future<List<MyUser>> getSearch(String query) async {
    
    var response = await http.get(Uri.parse('https://afternoon-coast-55254.herokuapp.com/?query=$query'));
    print(response.body);
    List<MyUser> jugadores = [];
    if ( response.statusCode == 200) {
      Iterable I = json.decode(response.body);
      jugadores = List<MyUser>.from(I.map((model)=> MyUser.fromJson(model)));
    }
    return jugadores;
  } 

}