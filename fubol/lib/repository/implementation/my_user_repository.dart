import 'package:fubol/model/user.dart';
import 'package:fubol/provider/firebase_provider.dart';
import 'dart:io';

import 'package:fubol/repository/my_user_repository.dart';

class MyUserRepository extends MyUserRepositoryBase {
  final provider = FirebaseProvider();

  @override
  Future<MyUser?> getMyUser() => provider.getMyUser();

  @override
  Future<void> saveMyUser(MyUser user, File? image) =>
      provider.saveMyUser(user, image);

  @override
  Future<List<MyUser>> getUsers() => provider.getUsers();
}
