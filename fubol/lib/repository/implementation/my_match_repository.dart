import 'package:fubol/model/match.dart';
import 'package:fubol/provider/firebase_provider.dart';
import 'package:fubol/repository/my_match_repository.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:vector_math/vector_math.dart' as math;
import 'dart:math';



class MyMatchRepository extends MyMatchRepositoryBase {
  final provider = FirebaseProvider();
  Location location = Location();  

  //@override
  //Future<void> saveMyTeam(MyTeam team, File? image) =>
      //provider.saveMyTeam(team, image);

  @override
  Future<List<MyMatch>> getMatchs() => provider.getMatch();
}
