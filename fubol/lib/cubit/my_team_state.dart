part of 'my_team_cubit.dart';

abstract class MyTeamState extends Equatable {
  @override
  List<Object?> get props => [];
}

class MyTeamInitial extends MyTeamState {}

class MyTeamLoadingState extends MyTeamState {}

class MyTeamReadyState extends MyTeamState {
  final MyTeam team;
  final bool isSaving;

  MyTeamReadyState(this.team,{this.isSaving = false});

  @override
  List<Object?> get props => [team,isSaving];
}

class TeamsReadyState extends MyTeamState {
  final List<MyTeam> teams;

  TeamsReadyState(this.teams);

  @override
  List<Object?> get props => [teams];
}
