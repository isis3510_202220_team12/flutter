// ignore_for_file: depend_on_referenced_packages

import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fubol/model/user.dart';
import 'package:fubol/repository/my_user_repository.dart';
part 'my_user_state.dart';

class MyUserCubit extends Cubit<MyUserState> {
  final MyUserRepositoryBase _userRepository;

  File? _pickedImage;
  late MyUser _user;
  late List<MyUser> _users;

  MyUserCubit(this._userRepository) : super(MyUserLoadingState());

  void setImage(File? imageFile) {
    _pickedImage = imageFile;
    emit(MyUserReadyState(_user, _pickedImage));
  }

  Future<void> getMyUser() async {
    emit(MyUserLoadingState());
    _user =
        (await _userRepository.getMyUser()) ?? MyUser(age: 0, name: "", email: "", typeUser: "Player", ranking: 0, description: "", isReferee: false, id:'');
    emit(MyUserReadyState(_user, _pickedImage));
  }

  Future<void> saveMyUser(
      String uid, String name, String email, int age, String typeUser, bool isReferee, String description, int ranking) async {
    _user = MyUser(age: age, description: description, email: email, isReferee: false, name: name, ranking: ranking, typeUser: typeUser, id: uid);
    emit(MyUserReadyState(_user, _pickedImage, isSaving: true));

    await _userRepository.saveMyUser(_user, _pickedImage);
    emit(MyUserReadyState(_user, _pickedImage));
  }

  Future<void>? getUsers() async {
    
    _users = await _userRepository.getUsers();
    emit(UsersReadyState(_users));
  }
}
