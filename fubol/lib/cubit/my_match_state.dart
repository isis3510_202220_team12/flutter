part of 'my_match_cubit.dart';

abstract class MyMatchState extends Equatable {
  @override
  List<Object?> get props => [];
}

class MyMatchInitial extends MyMatchState {}

class MyMatchLoadingState extends MyMatchState {}

class MyMatchReadyState extends MyMatchState {
  final MyMatch match;
  final bool isSaving;

  MyMatchReadyState(this.match,{this.isSaving = false});

  @override
  List<Object?> get props => [match,isSaving];
}

class MatchsReadyState extends MyMatchState {
  final List<MyMatch> matchs;

  MatchsReadyState(this.matchs);

  @override
  List<Object?> get props => [matchs];
}