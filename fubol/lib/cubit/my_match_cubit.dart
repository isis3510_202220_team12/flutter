import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fubol/model/match.dart';
import 'package:fubol/repository/my_match_repository.dart';
part 'my_match_state.dart';

class MyMatchCubit extends Cubit<MyMatchState> {
  final MyMatchRepositoryBase _matchRepository;

  late List<MyMatch> _matchs;

  MyMatchCubit(this._matchRepository) : super(MyMatchLoadingState());

  Future<void> getMatchs() async {
    _matchs = await _matchRepository.getMatchs();
    emit(MatchsReadyState(_matchs));
  }

}