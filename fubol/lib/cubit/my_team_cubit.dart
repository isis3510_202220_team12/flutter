// ignore_for_file: depend_on_referenced_packages

import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fubol/model/teams.dart';
import 'package:fubol/repository/my_team_repository.dart';
part 'my_team_state.dart';

class MyTeamCubit extends Cubit<MyTeamState> {
  final MyTeamRepositoryBase _teamRepository;

  late List<MyTeam> _teams;

  MyTeamCubit(this._teamRepository) : super(MyTeamLoadingState());

  Future<void> getTeams() async {
    _teams = await _teamRepository.getTeams();
    emit(TeamsReadyState(_teams));
  }

}
