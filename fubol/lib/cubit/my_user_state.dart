part of 'my_user_cubit.dart';

abstract class MyUserState extends Equatable {
  @override
  List<Object?> get props => [];
}

class MyUserInitial extends MyUserState {}

class MyUserLoadingState extends MyUserState {}

class MyUserReadyState extends MyUserState {
  final MyUser user;
  final File? pickedImage;
  final bool isSaving;

  MyUserReadyState(this.user, this.pickedImage, {this.isSaving = false});

  @override
  List<Object?> get props => [user, pickedImage?.path, isSaving];
}

class UsersReadyState extends MyUserState {
  final List<MyUser> users;

  UsersReadyState(this.users);

  @override
  List<Object?> get props => [users];
}
