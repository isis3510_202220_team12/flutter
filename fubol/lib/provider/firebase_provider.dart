import 'dart:io';
import 'dart:isolate';
import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:fubol/model/user.dart';
import 'package:fubol/model/teams.dart';
import 'package:fubol/model/match.dart';
import 'package:fubol/provider/conex_provider.dart';
// ignore: depend_on_referenced_packages
import 'package:path/path.dart' as path;

class FirebaseProvider {

  late StreamSubscription _connectionChangeStream;

  FirebaseProvider(){
    StreamSubscription _connectionChangeStream = ConnectionStatusSingleton.getInstance().connectionChange.listen(connectionChanged);
  }

  List<MyUser> usersCache = [];

  void connectionChanged(dynamic hasConnection) {
      isOffline = !hasConnection;
  }

  bool isOffline = false;

  User get currentUser {
    final user = FirebaseAuth.instance.currentUser;
    if (user == null) throw Exception('Not authenticated exception');
    return user;
  }

  FirebaseFirestore get firestore => FirebaseFirestore.instance;

  FirebaseStorage get storage => FirebaseStorage.instance;

  Future<MyUser?> getMyUser() async {
    final p = ReceivePort();
    
    final snapshot = await firestore.doc('user/${currentUser.uid}').get();
    if (snapshot.exists) return MyUser.fromFirebaseMap(snapshot.data()!);
    return null;
  }

  Future<void> saveMyUser(MyUser user, File? image) async {
    final ref = firestore.doc('user/${currentUser.uid}');
    if (image != null) {
      final imagePath =
          '${currentUser.uid}/profile/${path.basename(image.path)}';
      final storageRef = storage.ref(imagePath);
      await storageRef.putFile(image);
      final url = await storageRef.getDownloadURL();
      await ref.set(user.toFirebaseMap(newImage: url), SetOptions(merge: true));
    } else {
      await ref.set(user.toFirebaseMap(), SetOptions(merge: true));
    }
  }

  Future<List<MyUser>> getUsers() async {
    
    List<MyUser> users = [];
    print(isOffline);
    if(isOffline && usersCache.isNotEmpty){
      return usersCache;
    }
    else{
      try {

        final pla = await firestore.collection('user').get();
        // ignore: avoid_function_literals_in_foreach_calls
        pla.docs.forEach((element) {
          usersCache.add(MyUser.fromFirebaseMap(element.data()));
          return users.add(MyUser.fromFirebaseMap(element.data()));
        });
        return users;      
      
    } on FirebaseException catch (e) {
      if (kDebugMode) {
        print('Failed with error ${e.code}: ${e.message}');
      }
      return users;
    } catch (e) {
      throw Exception(e.toString());
    }
    }
  }

  Future<List<MyTeam>> getTeams() async {
    List<MyTeam> teams = [];
    try {
      final pla = await firestore.collection('team').get();
      // ignore: avoid_function_literals_in_foreach_calls
      pla.docs.forEach((element) {
        teams.add(MyTeam.fromFirebaseMap(element.data()));
      });
      
      return teams;
    } on FirebaseException catch (e) {
      if (kDebugMode) {
        print('Failed with error ${e.code}: ${e.message}');
      }
      return teams;
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<List<MyMatch>> getMatch() async {
    List<MyMatch> match = [];
    try {
      final pla = await firestore.collection('matchs').get();
      // ignore: avoid_function_literals_in_foreach_calls
      pla.docs.forEach((element) {
        match.add(MyMatch.fromFirebaseMap(element.data()));
      });
      
      return match;
    } on FirebaseException catch (e) {
      if (kDebugMode) {
        print('Failed with error ${e.code}: ${e.message}');
      }
      return match;
    } catch (e) {
      throw Exception(e.toString());
    }
  }
}
