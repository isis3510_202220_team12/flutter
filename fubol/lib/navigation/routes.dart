import 'package:flutter/material.dart';
import 'package:fubol/screen/home_screen.dart';
import 'package:fubol/screen/players_list_screen.dart';
import 'package:fubol/screen/profile_screen.dart';
import 'package:fubol/screen/auth/login_screen.dart';
import 'package:fubol/screen/auth/splash_screen.dart';
import 'package:fubol/screen/auth/register_screen.dart';
import 'package:fubol/screen/referee_list_screen.dart';
import 'package:fubol/screen/teams_list_screen.dart';
import 'package:fubol/screen/matchs_list_screen.dart';

class Routes {
  static const splash = '/';
  static const login = '/login';
  static const profile = '/profile';
  static const home = '/home';
  static const register = '/register';
  static const teams = '/teams';
  static const players = '/players';
  static const matchs = '/matchs';
  static const map = '/map';
  static const referee = '/referee';
  static Route routes(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case splash:
        return _buildRoute(SplashScreen.create);
      case login:
        return _buildRoute(LoginScreen.create);
      case profile:
        return _buildRoute(ProfileScreen.create);
      case home:
        return _buildRoute(HomeScreen.create);
      case register:
        return _buildRoute(RegisterScreen.create);
      case teams:
        return _buildRoute(TeamsScreen.create);
      case players:
        return _buildRoute(PlayersScreen.create);
      case matchs:
        return _buildRoute(MatchsScreen.create);
      case referee:
        return _buildRoute(RefereesScreen.create);
      default:
        throw Exception('Route does not exist');
    }
  }

  static MaterialPageRoute _buildRoute(Function build) =>
      MaterialPageRoute(builder: (context) => build(context));
}
