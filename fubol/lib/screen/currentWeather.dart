import 'dart:async';
import 'dart:convert';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fubol/model/wheather.dart';
import 'package:fubol/screen/local_map_screen.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../cubit/auth_cubit.dart';
import '../navigation/routes.dart';

class CurrentWeatherPage extends StatefulWidget {
  const CurrentWeatherPage({super.key});


  @override
  _CurrentWeatherPageState createState() => _CurrentWeatherPageState();
}

class _CurrentWeatherPageState extends State<CurrentWeatherPage> {
  late Weather _weather;
  var selectedIndex = 3;
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Weather Screen'),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout), onPressed: () {context.read<AuthCubit>().signOut();},
          ),
        ],
      ),
        body: Center(
          child: FutureBuilder(
            builder: (context, snapshot) {
              if (snapshot != null) {
                _weather = snapshot.data;
                if (_weather == null) {
                  return const Text("CHeck your connectivity");
                } else {
                  return  weatherBox(_weather);
                }
              } else {
                return const CircularProgressIndicator();
              }
            },
            future: getCurrentWeather(),
          ),
        ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.blue.withOpacity(.60),
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (int index) {
          // Respond to item press.
          setState(() {
            selectedIndex = index;
          });
          if (index == 4) {
            Navigator.pushNamed(context, Routes.profile);
          }
          else if (index == 1){
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const SearchPlacesScreen(),
            ));

          }
          else if (index == 2){
            Navigator.pushNamed(context, Routes.home);   
          }
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.emoji_events),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.map),

          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.cloud),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.account_circle),
          ),
        ],
        currentIndex: selectedIndex,
      ),
    );
  }
}
Widget weatherBox(Weather weather) {

  return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
            margin: const EdgeInsets.all(10.0),
            child:
            Text("${weather.temp}°C",
              textAlign: TextAlign.center,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 55),
            )
        ),
        Container(
            margin: const EdgeInsets.all(5.0),
            child: Text(weather.description)
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
          // ignore: prefer_const_constructors
          child: IconButton(
            icon: Image.asset(
              'lib/Images/WeatherLogo.png',
            ),
            iconSize: 150,
            onPressed: () {},
          ),
        ),
        Container(
            margin: const EdgeInsets.all(5.0),
            child: Text("Feels:${weather.feelsLike}°C")
        ),
        Container(
            margin: const EdgeInsets.all(5.0),
            child: Text("H:${weather.high}°C L:${weather.low}°C")
        ),
        
      ]
      

  );
}

Future getCurrentWeather() async {
    Weather weather;
    String city = "bogota";
    String apiKey = "665205fd74b7ffc328e72636328a9664";
    var url = "https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$apiKey&units=metric";

    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      weather = Weather.fromJson(jsonDecode(response.body));
      return weather;
    } else {
      // TODO: Throw error here
    }


  }
