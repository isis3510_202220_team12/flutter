import 'package:cache_manager/cache_manager.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fubol/cubit/auth_cubit.dart';
import 'package:fubol/cubit/my_match_cubit.dart';
import 'package:fubol/model/match.dart';
import 'package:fubol/navigation/routes.dart';
import 'package:fubol/repository/implementation/my_match_repository.dart';
import 'package:fubol/screen/currentWeather.dart';
import 'package:fubol/screen/local_map_screen.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:vector_math/vector_math.dart' as math;
import 'dart:math';
import 'package:latlng/latlng.dart';
import 'dart:isolate';
import 'dart:async';
import 'package:geolocator/geolocator.dart';


class MatchsScreen extends StatefulWidget {
  const MatchsScreen({super.key});

  static Widget create(BuildContext context) {
    return BlocProvider(
      create: (_) => MyMatchCubit(MyMatchRepository())..getMatchs(),
      child: const MatchsScreen(),
    );
  }

  @override
  State<MatchsScreen> createState() => _MatchsScreenState();
}

class _MatchsScreenState extends State<MatchsScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<MyMatchCubit, MyMatchState>(
        builder: (context, state) {
          if (state is MatchsReadyState) {
            return MatchsBuild(state.matchs);
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
class MatchsBuild extends StatefulWidget {
  final List<MyMatch> matchs;

  const MatchsBuild(this.matchs, {super.key});

  static Widget create(BuildContext context) {
    return BlocProvider(
      create: (_) => MyMatchCubit(MyMatchRepository())..getMatchs(),
      child: const MatchsScreen(),
    );
  }

  @override
  State<MatchsBuild> createState() => _MatchBuildState();
}

class _MatchBuildState extends State<MatchsBuild> {
  Isolate? isolate;

  late String _address,_dateTime;
  Location location = Location();
  LatLng _initialcameraposition = LatLng(0.5937, 0.962);
  LatLng actualLoc = LatLng(4.60142035, -74.0649170096208);
  
  
  static get dataMap => null;
  @override
  Widget build(BuildContext context) {
    var selectedIndex = 0;
    List<MyMatch> partidos = organizar(widget.matchs);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Matchs Screen'),
        actions: [
          IconButton(
            onPressed: () =>  DeleteCache.deleteKey("cache", context.read<AuthCubit>().signOut()),
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: partidos.length,
        itemBuilder: (context, index) {
          MyMatch match= partidos[index];
          if(partidos.isEmpty){
            showDialog(context: context, builder: (buildcontext) {
              return const AlertDialog(
                title: Text("No hay Partidos"),
                content: Text("Parece que no hay partidos cerca tuyo. Prueba más tarde cuando se creen nuevos."),
                actions: <Widget>[

                ],
              );
            }
          );

          }
          else if(partidos.isNotEmpty) {
            return buildMatch(match);
          }
          
          return const Text('');
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.blue.withOpacity(.60),
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (int index) {
          // Respond to item press.
          setState(() {
            selectedIndex = index;
          });
          if (index == 4) {
            Navigator.pushNamed(context, Routes.profile);
          }
          else if (index == 1){
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const SearchPlacesScreen(),
            ));

          }
          else if (index == 3){
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const CurrentWeatherPage(),
            ));
          }
          else if (index == 2){
            Navigator.pushNamed(context, Routes.home);
            
          }
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.emoji_events),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.map),

          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.cloud),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.account_circle),
          ),
        ],
        currentIndex: selectedIndex,
      ),
    );
  }

  List<MyMatch> organizar(List<MyMatch> lista){

      etLoc();
      List<MyMatch> filtrada = [];
      for (MyMatch partido in lista) {
        final ubi1 = partido.location.split(",");
        final lat1 = double.parse(ubi1.first);
        final lng1 = double.parse(ubi1.last);
        if(calculateDistance(lat1, lng1, actualLoc.latitude,actualLoc.longitude) < 1000){
          filtrada.add(partido);
        }
      }
      List<MyMatch> listaOrdenada = [];
      if(filtrada.isNotEmpty) {
        filtrada.sort(((a, b) => mySortComparison(b, a)));
        listaOrdenada = filtrada.reversed.toList();
      }
    
    return listaOrdenada;
  }

  double calculateDistance(double lat1, double lng1, double lat2, double lng2) {
    int radiusEarth = 6371;
    double distanceKm;
    double distanceMts;
    double dlat, dlng;
    double a;
    double c;

    //Convertimos de grados a radianes
    lat1 = math.radians(lat1);
    lat2 = math.radians(lat2);
    lng1 = math.radians(lng1);
    lng2 = math.radians(lng2);
    // Fórmula del semiverseno
    dlat = lat2 - lat1;
    dlng = lng2 - lng1;
    a = sin(dlat / 2) * sin(dlat / 2) +
        cos(lat1) * cos(lat2) * (sin(dlng / 2)) * (sin(dlng / 2));
    c = 2 * atan2(sqrt(a), sqrt(1 - a));

    distanceKm = radiusEarth * c;

    distanceMts = 1000 * distanceKm;

    return distanceKm;
    //return distanceMts;
  }

  int mySortComparison(MyMatch a, MyMatch b) {

    final ubi1 = a.location.split(",");
    final ubi2 = b.location.split(",");
    final lat1 = double.parse(ubi1.first);
    final lng1 = double.parse(ubi1.last);

    final lat2 = double.parse(ubi2.first);
    final lng2 = double.parse(ubi2.last);
//Se calculan las distancias respecto a la ubicación actual

    
    final propertyA = calculateDistance(lat1, lng1, actualLoc.latitude,actualLoc.longitude);
    final propertyB = calculateDistance(lat2, lng2, actualLoc.latitude,actualLoc.longitude);
    if (propertyA < propertyB) {
      return -1;
    } else if (propertyA > propertyB) {
      return 1;
    } else {
      return 0;
    }
  }

  etLoc() async{
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
  }



}

Widget buildMatch(MyMatch match) => ListTile(
      
      title: Text('${match.team1} ' '${match.team2}'),
      
      subtitle: Text(DateFormat('yyyy-MM-dd').format(match.dateMatch)),
    );



