import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fubol/navigation/routes.dart';
import '../../cubit/auth_cubit.dart';
import '../../cubit/auth_state.dart';
import 'package:hive/hive.dart';



class RegisterScreen extends StatelessWidget {
  const RegisterScreen({super.key});

  static Widget create(BuildContext context) => const RegisterScreen();

  @override
  Widget build(BuildContext context) {

    final formKey = GlobalKey<FormState>();
    final nameController = TextEditingController();
    final emailController = TextEditingController();
    final passwordController = TextEditingController();
    final repeatPasswordController = TextEditingController();
    final ageController = TextEditingController();
    final descriptionController = TextEditingController();
    final rankingController = TextEditingController();

    
    final myBox = Hive.box("form");

    // ignore: non_constant_identifier_names
    void StoreLocaly(){
      Hive.openBox("form");
      //Local Storage 2
      _myBox.put(1, nameController.text);
      _myBox.put(2, emailController.text);
      _myBox.put(3, passwordController.text);
      _myBox.put(4, ageController.text);
      _myBox.put(5, descriptionController.text);
      _myBox.put(6, rankingController.text);
    }

    // ignore: non_constant_identifier_names
    void ClearLocaly(){
      print("pinguino");
      Hive.openBox("form");
      myBox.delete(1);

      myBox.delete(2);

      myBox.delete(3);

      myBox.delete(4);

      _myBox.delete(5);

      _myBox.delete(6);   

      myBox.delete(6);   
      print(myBox.get(6, defaultValue: ''));
    }

    // ignore: non_constant_identifier_names
    void LoadLocal(){
      Hive.openBox("form");
      nameController.text = myBox.get(1, defaultValue: '');
      emailController.text = myBox.get(2, defaultValue: '');
      passwordController.text = myBox.get(3, defaultValue: '');
      ageController.text = myBox.get(4, defaultValue: '');
      descriptionController.text = myBox.get(5, defaultValue: '');
      rankingController.text = myBox.get(6, defaultValue: '');
    }
    LoadLocal();
      
    final isSigningIn = context.watch<AuthCubit>().state is AuthSigningIn;

    String? emailValidator(String? value) {
      StoreLocaly();
      if (value == null || value.isEmpty) return 'This is a required field';
      if (!EmailValidator.validate(value)) return 'Enter a valid email';
      return null;
    }

    String? passwordValidator(String? value) {
      StoreLocaly();
      if (value == null || value.isEmpty) return 'This is a required field';
      if (value.length < 6) return 'Password should be at least 6 letters';
      if (passwordController.text != repeatPasswordController.text) {
        return 'Password do not match';
      }
      return null;
    }

    String? requiredValidator(String? value) {
      StoreLocaly();
      if (value == null || value.isEmpty) return 'This is a required field';
      return null;
    }


    return AbsorbPointer(
      
      absorbing: isSigningIn,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Register User'),
        ),
        body: BlocBuilder<AuthCubit, AuthState>(
          builder: (context, state) {
            return Container(
              padding: const EdgeInsets.all(24),
              child: Center(
                child: ListView(
                  children: [
                    Form(
                      key: formKey,
                      child: Column(
                        children: [
                          TextFormField(
                            
                            controller: nameController,
                            maxLength: 30,
                            decoration:
                                const InputDecoration(labelText: 'Full Name'),
                            validator: requiredValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            

                          ),
                          TextFormField(
                            controller: emailController,
                            maxLength: 30,
                            decoration:
                                const InputDecoration(labelText: 'Email'),
                            validator: emailValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                          ),
                          TextFormField(
                            controller: passwordController,
                            maxLength: 30,
                            decoration:
                                const InputDecoration(labelText: 'Password'),
                            validator: passwordValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            obscureText: true,
                          ),
                          TextFormField(
                            controller: repeatPasswordController,
                            maxLength: 30,
                            decoration: const InputDecoration(
                                labelText: 'Repeat Password'),
                            validator: passwordValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            obscureText: true,
                          ),
                          TextFormField(
                            controller: ageController,
                            maxLength: 3,
                            decoration: const InputDecoration(labelText: 'Age'),
                            validator: requiredValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            keyboardType: TextInputType.number,
                          ),
                          TextFormField(
                            controller: descriptionController,
                            maxLength: 500,
                            decoration: const InputDecoration(labelText: 'add your description'),
                            validator: requiredValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                          ),
                          TextFormField(
                            controller: rankingController,
                            maxLength: 3,
                            decoration: const InputDecoration(labelText: 'ranking'),
                            validator: requiredValidator,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            keyboardType: TextInputType.number,    
                          ),
                        ],
                      ),
                    ),
                    if (isSigningIn)
                      Container(
                        alignment: Alignment.center,
                        width: 50,
                        child: const CircularProgressIndicator(),
                      ),
                    if (state is AuthError)
                      Text(
                        state.message,
                        style: const TextStyle(
                            color: Colors.red,
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize: const Size.fromHeight(50),
                      ),
                      child: const Text('Register'),
                      onPressed: () {
                        
                        if (formKey.currentState?.validate() == true) {
                          context.read<AuthCubit>().registerUser(
                                nameController.text,
                                emailController.text,
                                passwordController.text,
                                int.tryParse(ageController.text) ?? 0,
                                descriptionController.text,
                                int.tryParse(rankingController.text) ?? 0,
                              );
                          ClearLocaly();
                        }
                      },
                    ),
                    Row(
                      // ignore: sort_child_properties_last
                      children: <Widget>[
                        const Text(
                          'Already have an account?',
                          style: TextStyle(color: Colors.grey),
                        ),
                        TextButton(
                          onPressed: () {
                            context.read<AuthCubit>().reset();
                            Navigator.pushNamed(context, Routes.login);
                          },
                          child: const Text(
                            'LogIn',
                          ),
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );  
  }  
}


