import 'package:cache_manager/cache_manager.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fubol/cubit/auth_cubit.dart';
import 'package:fubol/navigation/routes.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:hive/hive.dart';

import '../../cubit/auth_state.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  static Widget create(BuildContext context) => const LoginScreen();

  @override
  Widget build(BuildContext context) {
    FirebaseAnalytics analytics = FirebaseAnalytics.instance;
    final formKey = GlobalKey<FormState>();
    final emailController = TextEditingController();
    final passwordController = TextEditingController();

    final isSigningIn = context.watch<AuthCubit>().state is AuthSigningIn;

    String? emailValidator(String? value) {
      if (value == null || value.isEmpty) return 'This is a required field';
      return null;
    }

    String? passwordValidator(String? value) {
      if (value == null || value.isEmpty) return 'This is a required field';
      return null;
    }


    Future<void> signUpEvent() async{
      await FirebaseAnalytics.instance.logSignUp(signUpMethod: 'Inicio sesión desde main');
      print('evento');
    }

    return AbsorbPointer(
      absorbing: isSigningIn,
      child: Scaffold(
        body: BlocBuilder<AuthCubit, AuthState>(
          builder: (context, state) {
            return Container(
              padding: const EdgeInsets.all(24),
              child: Center(
                child: ListView(
                  children: [
                    Image.asset(
                      'lib/Images/FubolLogo.png',
                      height: 150,
                      width: 150,
                    ),
                    Form(
                      key: formKey,
                      child: Column(children: [
                        TextFormField(
                          controller: emailController,
                          maxLength: 50,
                          decoration: const InputDecoration(labelText: 'Email'),
                          validator: emailValidator,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                        ),
                        TextFormField(
                          controller: passwordController,
                          maxLength: 50,
                          decoration:
                              const InputDecoration(labelText: 'Password'),
                          validator: passwordValidator,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          obscureText: true,
                        ),
                      ]),
                    ),
                    if (isSigningIn)
                      Container(
                        alignment: Alignment.center,
                        width: 50,
                        child: const CircularProgressIndicator(),
                      ),
                    if (state is AuthError)
                      Text(
                        state.message,
                        style: const TextStyle(
                            color: Colors.red,
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    Builder(
                      builder: (context) => TextButton(
                        onPressed: () {},
                        child: Text(
                          'Forgot Password?',
                          style: TextStyle(color: Colors.grey[600]),
                        ),
                      ),
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: const Size.fromHeight(50),
                        ),
                        child: const Text('Log In'),
                        onPressed: () {
                          if (formKey.currentState?.validate() == true) {
                            signUpEvent();
                            Future initCache() async{
                              CacheManagerUtils.conditionalCache(key: "cache", valueType: ValueType.StringValue, 
                              actionIfNull: (){
                                context.read<AuthCubit>().logInUser(
                                  emailController.text,
                                  passwordController.text,
                                );
                              }, actionIfNotNull: (){
                                print("El cache debería estar vacío");
                              });
                            }
                            initCache();
                            
                          }
                        }),
                    Row(
                      // ignore: sort_child_properties_last
                      children: <Widget>[
                        const Text(
                          'Does not have account?',
                          style: TextStyle(color: Colors.grey),
                        ),
                        TextButton(
                          onPressed: () {
                            context.read<AuthCubit>().reset();
                            Navigator.pushNamed(context, Routes.register);
                          },
                          child: const Text(
                            'Sign Up',
                          ),
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
