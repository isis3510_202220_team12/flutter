import 'dart:io';

import 'package:cache_manager/cache_manager.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fubol/cubit/auth_cubit.dart';
import 'package:fubol/cubit/my_user_cubit.dart';
import 'package:fubol/model/user.dart';
import 'package:fubol/navigation/routes.dart';
import 'package:fubol/repository/implementation/my_user_repository.dart';
import 'package:fubol/screen/currentWeather.dart';
import 'package:fubol/screen/local_map_screen.dart';
import 'package:image_picker/image_picker.dart';

import '../cubit/auth_state.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  static Widget create(BuildContext context) {
    return BlocProvider(
      create: (_) => MyUserCubit(MyUserRepository())..getMyUser(),
      child: const ProfileScreen(),
    );
  }

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    var selectedIndex = 4;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Welcome'),
        actions: [
          IconButton(
            onPressed: () =>  DeleteCache.deleteKey("cache", context.read<AuthCubit>().signOut()),
            icon: const Icon(Icons.logout),
          )
        ],
      ),
      body: BlocBuilder<MyUserCubit, MyUserState>(
        builder: (context, state) {
          if (state is MyUserReadyState) {
            return _MyUserSection(
              user: state.user,
              pickedImage: state.pickedImage,
              isSaving: state.isSaving,
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.blue.withOpacity(.60),
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (int index) {
          // Respond to item press.
          setState(() {
            selectedIndex = index;
          });
          if (index == 4) {
            Navigator.pushNamed(context, Routes.profile);
          }
          else if (index == 1){
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const SearchPlacesScreen(),
            ));

          }
          else if (index == 2){
            Navigator.pushNamed(context, Routes.home);
          }
          else if (index == 3){
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const CurrentWeatherPage(),
            ));

          }
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.emoji_events),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.map),

          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.cloud),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.account_circle),


          ),
        ],
        currentIndex: selectedIndex,
      ),
    );
  }
}

class _MyUserSection extends StatefulWidget {
  final MyUser? user;
  final File? pickedImage;
  final bool isSaving;

  const _MyUserSection({this.user, this.pickedImage, this.isSaving = false});

  @override
  State<_MyUserSection> createState() => __MyUserSectionState();
}

class __MyUserSectionState extends State<_MyUserSection> {
  final _idController = TextEditingController();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _ageController = TextEditingController();
  final _typeUserController = TextEditingController();
  final _isRefereeController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _rankingController = TextEditingController();

  final picker = ImagePicker();

  @override
  void initState() {
    _idController.text = widget.user?.id ?? '';
    _nameController.text = widget.user?.name ?? '';
    _emailController.text = widget.user?.email ?? '';
    _ageController.text = widget.user?.age.toString() ?? '';
    _typeUserController.text = widget.user?.typeUser ?? '';
    _isRefereeController.text = widget.user?.isReferee.toString() ?? '';
    _descriptionController.text = widget.user?.description ?? '';
    _rankingController.text = widget.user?.ranking.toString() ?? '';

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget image = Image.asset(
      'lib/Images/IconLogo.png',
      fit: BoxFit.fill,
    );

    if (widget.pickedImage != null) {
      image = Image.file(
        widget.pickedImage!,
        fit: BoxFit.fill,
      );
    } else if (widget.user?.image != null && widget.user!.image!.isNotEmpty) {
      image = CachedNetworkImage(
        imageUrl: widget.user!.image!,
        progressIndicatorBuilder: (_, __, progress) =>
            CircularProgressIndicator(value: progress.progress),
        errorWidget: (_, __, ___) => const Icon(Icons.error),
        fit: BoxFit.fill,
      );
    }

    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () async {
                final myUserCubit = context.read<MyUserCubit>();
                final pickedImage =
                    await picker.pickImage(source: ImageSource.gallery);
                if (pickedImage != null) {
                  myUserCubit.setImage(File(pickedImage.path));
                }
              },
              child: Center(
                child: ClipOval(
                  child: SizedBox(
                    width: 150,
                    height: 150,
                    child: image,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            const SizedBox(
              height: 8,
            ),
            TextField(
              controller: _nameController,
              decoration: const InputDecoration(labelText: 'Name'),
              maxLength: 30,
            ),
            TextField(
              controller: _emailController,
              decoration: const InputDecoration(labelText: 'Email'),
              enabled: false,
            ),
            TextField(
              controller: _ageController,
              decoration: const InputDecoration(labelText: 'Age'),
              maxLength: 3,
              keyboardType: TextInputType.number,
            ),
            TextField(
              controller: _isRefereeController,
              decoration: const InputDecoration(labelText: 'Are you a referee?'),
              maxLength: 5,
            ),
            TextField(
              controller: _descriptionController,
              decoration: const InputDecoration(labelText: 'Description'),
              maxLength: 500,
            ),TextField(
              controller: _rankingController,
              decoration: const InputDecoration(labelText: 'Ranking'),
              maxLength: 3,
              keyboardType: TextInputType.number,
            ),
            const SizedBox(
              height: 15,
            ),
            SizedBox(
              width: 200,
              child: ElevatedButton(
                onPressed: widget.isSaving
                    ? null
                    : () async {
                        await context.read<MyUserCubit>().saveMyUser(
                            (context.read<AuthCubit>().state as AuthSignedIn)
                                .user
                                .uid,
                            _nameController.text,
                            widget.user!.email,
                            int.tryParse(_ageController.text) ?? 0,
                            'Player', true, _descriptionController.text, int.tryParse(_rankingController.text) ?? 0);

                        // ignore: use_build_context_synchronously
                        Navigator.pushNamed(context, Routes.home);

                        // ignore: use_build_context_synchronously
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text('User Information Updated Correctly'),
                          ),
                        );
                      },
                child: const Text('Update Information'),
              ),
            ),
            if (widget.isSaving) const CircularProgressIndicator(),
          ],
        ),
      ),
    );
  }
}
