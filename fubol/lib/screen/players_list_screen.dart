import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cache_manager/cache_manager.dart';
import 'package:fubol/cubit/auth_cubit.dart';
import 'package:fubol/cubit/my_user_cubit.dart';
import 'package:fubol/model/user.dart';
import 'package:fubol/navigation/routes.dart';
import 'package:fubol/repository/implementation/my_user_repository.dart';
import 'package:fubol/screen/currentWeather.dart';
import 'package:fubol/screen/local_map_screen.dart';
import 'package:fubol/utils/CustomSearchDelegate.dart';

class PlayersScreen extends StatefulWidget {
  const PlayersScreen({super.key});

  static Widget create(BuildContext context) {
    return BlocProvider(
      create: (_) => MyUserCubit(MyUserRepository())..getUsers(),
      child: const PlayersScreen(),
    );
  }

  @override
  State<PlayersScreen> createState() => _PlayersScreenState();
}

class _PlayersScreenState extends State<PlayersScreen> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<MyUserCubit, MyUserState>(
        builder: (context, state) {
          if (state is UsersReadyState) {
            return PlayersBuild(state.users);
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class PlayersBuild extends StatefulWidget {
  final List<MyUser> users;

  const PlayersBuild(this.users, {super.key});

  static Widget create(BuildContext context) {
    return BlocProvider(
      create: (_) => MyUserCubit(MyUserRepository())..getUsers(),
      child: const PlayersScreen(),
    );
  }

  @override
  State<PlayersBuild> createState() => _PlayersBuildState();
}

class _PlayersBuildState extends State<PlayersBuild> {
  @override
  Widget build(BuildContext context) {
    var selectedIndex = 0;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Players Screen'),
        actions: [
          IconButton(
            onPressed: () =>  DeleteCache.deleteKey("cache", context.read<AuthCubit>().signOut()),
            icon: const Icon(Icons.logout),
          ),
          
        ],
        leading: IconButton(
            onPressed: () {
              // method to show the search bar
              showSearch(
                context: context,
                // delegate to customize the search bar
                delegate: CustomSearchDelegate()
              );
            },
            icon: const Icon(Icons.search),
          ),
      ),
      
      body: ListView.builder(
        itemCount: widget.users.length,
        itemBuilder: (context, index) {
          MyUser player = widget.users[index];
          
          if (player.typeUser == 'Player') {
            
            return buildPlayer(player);
          }
          return const Text('');
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.blue.withOpacity(.60),
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (int index) {
          // Respond to item press.
          setState(() {
            selectedIndex = index;
          });
          if (index == 4) {
            Navigator.pushNamed(context, Routes.profile);
          }
          else if (index == 1){
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const SearchPlacesScreen(),
            ));

          }
          else if (index == 2){
            Navigator.pushNamed(context, Routes.home);
          }
          else if (index == 3){
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const CurrentWeatherPage(),
            ));

          }
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.emoji_events),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.map),

          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.cloud),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.account_circle),


          ),
        ],
        currentIndex: selectedIndex,
      ),
    );
  }
}

Widget buildPlayer(MyUser player) => ListTile(
      leading: CircleAvatar(
          backgroundColor: Colors.white,
          child: ClipOval(
            child: profileImageIcon(player),
          )),
      title: Text(player.name),
      subtitle: Text(player.email),
    );


Widget profileImageIcon(MyUser player) {
  Widget image = Image.asset(
    'lib/Images/IconLogo.png',
    fit: BoxFit.fill,
  );
  if (player.image != null && player.image!.isNotEmpty) {
    image = CachedNetworkImage(
      imageUrl: player.image!,
      progressIndicatorBuilder: (_, __, progress) =>
          CircularProgressIndicator(value: progress.progress),
      errorWidget: (_, __, ___) => const Icon(Icons.error),
      fit: BoxFit.fill,
    );
  }
  return image;
}


