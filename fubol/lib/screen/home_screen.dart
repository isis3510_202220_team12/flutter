import 'package:cache_manager/cache_manager.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fubol/cubit/auth_cubit.dart';
import 'package:fubol/cubit/my_user_cubit.dart';
import 'package:fubol/model/user.dart';
import 'package:fubol/navigation/routes.dart';
import 'package:fubol/repository/implementation/my_user_repository.dart';
import 'package:fubol/screen/currentWeather.dart';
import 'package:fubol/screen/local_map_screen.dart';
import 'package:fubol/model/wheather.dart';
import 'package:fubol/screen/referee_list_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  static Widget create(BuildContext context) {
    return BlocProvider(
      create: (_) => MyUserCubit(MyUserRepository())..getMyUser(),
      child: const HomeScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<MyUserCubit, MyUserState>(
        builder: (context, state) {
          if (state is MyUserReadyState) {
            return HomeBuild(state.user);
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class HomeBuild extends StatefulWidget {
  final MyUser user;

  const HomeBuild(this.user, {super.key});

  @override
  State<HomeBuild> createState() => _HomeBuildState();
}

class _HomeBuildState extends State<HomeBuild> {
  @override
  Widget build(BuildContext context) {
    Widget image = Image.asset(
      'lib/Images/IconLogo.png',
      fit: BoxFit.fill,
    );
    if (widget.user.image != null && widget.user.image!.isNotEmpty) {
      image = CachedNetworkImage(
        imageUrl: widget.user.image!,
        progressIndicatorBuilder: (_, __, progress) =>
            CircularProgressIndicator(value: progress.progress),
        errorWidget: (_, __, ___) => const Icon(Icons.error),
        fit: BoxFit.fill,
      );
    }

    var selectedIndex = 2;
    return Scaffold(
      appBar: AppBar(
        leading: Center(
          child: ClipOval(
            child: SizedBox(
              child: IconButton(
                icon: image,
                onPressed: () {
                  Navigator.pushNamed(context, Routes.profile);
                },
              ),
            ),
          ),
        ),
        title: Text(widget.user.name),
        actions: [
          IconButton(
            onPressed: () 
            => DeleteCache.deleteKey("cache", context.read<AuthCubit>().signOut()),
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Wrap(
              children: [
                ClipOval(
                  child: SizedBox(
                    child: IconButton(
                      icon: Image.asset(
                        'lib/Images/TeamsLogo.png',
                        fit: BoxFit.fill,
                      ),
                      iconSize: 100,
                      onPressed: () {
                        Navigator.pushNamed(context, Routes.teams);
                      },
                    ),
                  ),
                ),
                const SizedBox(
                  width: 80,
                ),
                ClipOval(
                  child: SizedBox(
                    child: IconButton(
                      icon: Image.asset(
                        'lib/Images/MatchesLogo.png',
                        fit: BoxFit.fill,
                      ),
                      iconSize: 100,
                      onPressed: () {
                        Navigator.pushNamed(context, Routes.matchs);
                      },
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            ClipOval(
              child: SizedBox(
                child: IconButton(
                  icon: Image.asset(
                    'lib/Images/FubolLogo.png',
                    fit: BoxFit.fill,
                  ),
                  iconSize: 150,
                  onPressed: () {},
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Wrap(
              children: [
                ClipOval(
                  child: SizedBox(
                    child: IconButton(
                      icon: Image.asset(
                        'lib/Images/PlayersLogo.png',
                        fit: BoxFit.fill,
                      ),
                      iconSize: 100,
                      onPressed: () {
                        Navigator.pushNamed(context, Routes.players);
                      },
                    ),
                  ),
                ),
                const SizedBox(
                  width: 80,
                ),
                ClipOval(
                  child: SizedBox(
                    child: IconButton(
                      icon: Image.asset(
                        'lib/Images/RefereesLogo.png',
                        fit: BoxFit.fill,
                      ),
                      iconSize: 100,
                      onPressed: () {},
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.blue.withOpacity(.60),
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (int index) {
          // Respond to item press.
          setState(() {
            selectedIndex = index;
          });
          if (index == 4) {
            Navigator.pushNamed(context, Routes.profile);
          }
          else if (index == 1){
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const SearchPlacesScreen(),
            ));

          }
          else if (index == 3){
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const CurrentWeatherPage(),
            ));
          }
          else if (index == 4){
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const RefereesScreen(),
                ));
          }
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.emoji_events),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.map),

          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.cloud),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.account_circle),


          ),
        ],
        currentIndex: selectedIndex,
      ),
    );
  }
}
