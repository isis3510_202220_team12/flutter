import 'dart:async';

import 'package:cache_manager/cache_manager.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fubol/cubit/auth_cubit.dart';
import 'package:fubol/cubit/my_team_cubit.dart';
import 'package:fubol/model/teams.dart';
import 'package:fubol/navigation/routes.dart';
import 'package:fubol/repository/implementation/my_team_repository.dart';
import 'package:fubol/provider/conex_provider.dart';
import 'package:fubol/screen/currentWeather.dart';
import 'package:fubol/screen/local_map_screen.dart';

class TeamsScreen extends StatefulWidget {
  const TeamsScreen({super.key});

  static Widget create(BuildContext context) {
    return BlocProvider(
      create: (_) => MyTeamCubit(MyTeamRepository())..getTeams(),
      child: const TeamsScreen(),
    );
  }

  @override
  State<TeamsScreen> createState() => _TeamsScreenState();
}

class _TeamsScreenState extends State<TeamsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<MyTeamCubit, MyTeamState>(
        builder: (context, state) {
          if (state is TeamsReadyState) {
            return TeamsBuild(state.teams);
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class TeamsBuild extends StatefulWidget {
  final List<MyTeam> teams;

  const TeamsBuild(this.teams, {super.key});

  static Widget create(BuildContext context) {
    return BlocProvider(
      create: (_) => MyTeamCubit(MyTeamRepository())..getTeams(),
      child: const TeamsScreen(),
    );
  }

  @override
  State<TeamsBuild> createState() => _TeamBuildState();
}

class _TeamBuildState extends State<TeamsBuild> {


  @override
  Widget build(BuildContext context) {
    var selectedIndex = 0;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Teams Screen'),
        actions: [
          IconButton(
            onPressed: () =>  DeleteCache.deleteKey("cache", context.read<AuthCubit>().signOut()),
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: widget.teams.length,
        itemBuilder: (context, index) {
          MyTeam team= widget.teams[index];
          if (team.userType == 'Team') {
            return buildTeam(team);
          }

          return const Text('');
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.blue.withOpacity(.60),
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (int index) {
          // Respond to item press.
          setState(() {
            selectedIndex = index;
          });
          if (index == 4) {
            Navigator.pushNamed(context, Routes.profile);
          }
          else if (index == 1){
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const SearchPlacesScreen(),
            ));

          }
          else if (index == 2){
            Navigator.pushNamed(context, Routes.home);
          }
          else if (index == 3){
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const CurrentWeatherPage(),
            ));

          }
        },
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.emoji_events),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.map),

          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.cloud),
          ),
          BottomNavigationBarItem(
            label: (''),
            icon: Icon(Icons.account_circle),


          ),
        ],
        currentIndex: selectedIndex,
      ),
    );
  }
}

Widget buildTeam(MyTeam team) => ListTile(
      
      title: Text(team.name),
      
      subtitle: Text('${team.ranking}'),
    );


  

