import 'package:equatable/equatable.dart';
class MyTeam extends Equatable{

  final String name;
  final String captain;
  final String commentaries;
  final String id;
  final int ranking;
  final String userType;


  const MyTeam(
    
    this.name,
    this.captain,
    this.commentaries,
    this.id,
    this.ranking,
    this.userType,
  );

  @override
    // TODO: implement props
    List<Object?> get props => [];

  Map<String, Object?> toFirebaseMap() {
    return <String, Object?>{
      'name': name,
      'captain': captain,
      'commentaries': commentaries,
      'id': id,
      'ranking': ranking,
      'userType': userType,
    };
  }

  factory MyTeam.fromFirebaseMap(Map<String, Object?> data){return MyTeam(data['name'] as String, 
  data['captain'] as String, data['commentaries'] as String, data['id'] as String, data['ranking'] as int, data['userType'] as String);}
   
}