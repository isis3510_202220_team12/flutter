import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
class MyMatch extends Equatable{

  final DateTime dateMatch;
  final String id;
  final String location;
  final String referee;
  final String team1;
  final String team2;
  final String userType;


  const MyMatch(
    this.dateMatch,
    this.id,
    this.location,
    this.referee,
    this.team1,
    this.team2,
    this.userType,
  );

  @override
    // TODO: implement props
    List<Object?> get props => [];

  Map<String, Object?> toFirebaseMap() {
    return <String, Object?>{
      'dateMatch': dateMatch,
      'id': id,
      'location': location,
      'referee': referee,
      'team1': team1,
      'team2': team2,
      'userType': userType,
    };
  }

  factory MyMatch.fromFirebaseMap(Map<String, Object?> data){return MyMatch((data['dateMatch'] as Timestamp).toDate(), 
  data['id'] as String, data['location'] as String, data['referee'] as String, data['team1'] as String, 
  data['team2'] as String, data['userType'] as String);}
   
}